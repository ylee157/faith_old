import Armory
import asyncio
import aiofiles
import json
import pprint

raiders = ["asawbones", "bakuya", "chirco", "doinystus", "drakania",
    "falivash","ivy", "juvelices", "kageromaru", "opdhealer",
    "phaet", "rosaree", "temo","yaois", "zentauro", "sobored",
    "yvis", "alinali", "divinechaios", "jaydenstorm", "avarasong",
    "kanshou", "grimzo", "cancer", "ezmë", "deadeyedaisy", "krev",
    "mochii", "monkation", "yourloved", "premium", "hannuan",
    "skepsikyma", "kalystia", "ezzlok", "krapurpantzz",
    "gearteeth", "zeeandee"]

tierInfo = eval(open("tierInfo.dat", "r").read())

async def save(fName = "tierInfo.dat"):
    async with aiofiles.open(fName, "w") as f:
        pp = pprint.PrettyPrinter(indent = 4)
        await f.write(pp.pformat(tierInfo) + "\n")

async def remakeRecord(rList = raiders, fName = "tierInfo.dat"):
    #This code just got 100% more idiot-proof
    async with aiofiles.open("oldInfo.dat", "w") as oldrecord:
        pp = pprint.PrettyPrinter(indent = 4, stream = oldrecord)
        pp.pprint(tierInfo)
    tierInfo.clear()
    for r in rList:
        print("searching up " + r + "...")
        try:
            tierInfo[r] = await Armory.getTierPieces(r)
        except RuntimeError:
            print(r + " not found.")
    await save(fName)

nickname = {"sawbones": "asawbones",
            "bones": "asawbones",
            "zee": "zeeandee",
            "rose": "rosaree",
            "ros": "rosaree",
            "ezme": "ezmë",
            "opd": "opdhealer",
            "op": "opdhealer",
            "divine": "divinechaios",
            "avara": "avarasong",
            "daisy": "deadeyedaisy",
            "deadeye": "deadeyedaisy",
            "kaly": "kalystia",
            "sharla": "sharlá"}

def overWrite(p1, p2):
    if p2 == None:
        return True
    dLvl = {"LFR": 1, "normal": 2, "heroic": 3, "mythic": 4}
    if dLvl[p1["from"]] > dLvl[p2["from"]]:
        return True
    if "IL" not in p1:
        return False
    if ("IL" in p1 and "IL" not in p2) or p1["IL"] > p2["IL"]:
        return True
    return False

async def addPlayer(pName):
    r = pName.lower()
    tierInfo[r] = await Armory.getTierPieces(r) #this COULD error
    await save()

async def removePlayer(pName):
    r = pName.lower()
    del tierInfo[r]
    await save()

async def XWonY(x, slot, item):
    x = nickname[x] if x in nickNames else x
    p2 = tierInfo[x][slot]
    if overWrite(item, p2):
        tierInfo[x][slot] = item

async def nameWon(name, difficulty, slot, il = None)
    item = {'from': difficulty}
    if il != None:
        item["IL"] = il
    XWonY(name, slot, item)
