import requests
import asyncio
from datetime import datetime
session = requests.Session()
url = "https://www.warcraftlogs.com:443/v1/"

session.params.update({'api_key':
                 '3f248bda7726441ab32651d94f98eeea'})

def lastRaid():
    reports = []
    r = session.get(url + "reports/guild/Unified/Madoran/US")
    for item in r.json():
        reports.append(item)
    return reports[-1]['id']

class Night():

    def __init__(self, idIn):
        self.nightID = idIn
        self.pLst = []
        self.pDict = {}
        self.pulls = []

        response = session.get(url + "report/fights/" + self.nightID)
        if not response.ok:
            raise RuntimeError("Unable to fetch the log.")
        r = response.json()
        for entity in r["friendlies"] :
            if all(key != entity["type"]
                        for key in ["Pet", "NPC"]):
                self.pLst.append(entity)
                self.pDict[entity["id"]] = entity["name"]
        for pull in r["fights"]:
            if (pull['boss'] != 0
                and pull['end_time'] - pull['start_time']
                    > 30000):
                self.pulls.append(pull)
        if len(self.pulls) == 0:
            raise RuntimeError("Empty log")
        self.title = r["title"]
        self.time = r["start"]

    def start(self, pull = -1):
        return self.pulls[pull]['start_time']

    def end(self, pull = -1):
        return self.pulls[pull]['end_time']

    def __str__(self):
        return self.title + datetime.fromtimestamp(
                    self.time/1000).strftime(" on %a, %m/%d")

    def pullDesc(self, pullNum = -1):
        if pullNum >= len(self.pulls):
            return "We didn't do that many pulls."
        pull = self.pulls[pullNum]
        boss = pull["name"]
        killStr = " kill on " if pull["kill"] else " wipe on "
        timeStr = datetime.fromtimestamp(
                (self.time + self.end(pullNum))/1000).strftime("%H:%M.")
        return boss + killStr + timeStr

    tankSpecID = [66, 73, 104, 250, 268, 581]
    healSpecID = [65, 105, 256, 257, 264, 270]

    def dpsLst(self, pull = -1):
        start = self.start(pull)
        response = session.get(url + "report/events/" + self.nightID,
                    params = {"start": start, "end": start + 1})

        if not response.ok:
            raise RuntimeError("Connection Interrupted at dpsLst")

        events = response.json()["events"]
        lst = []
        for e in events:
            etype = e["type"]
            if etype == "encounterstart":
                continue
            elif etype == "combatantinfo":
                specID = e["specID"]
                if (specID not in Night.tankSpecID
                        and specID not in Night.healSpecID):
                    lst.append(e["sourceID"])
            else:
                break
        return lst

    # def potionCount(self, pID, pull = -1):
    #     response = session.get(url + "report/tables/buffs/" + self.nightID,
    #             params = {"start": self.start(pull),
    #                       "end": self.end(pull), "targetid": pID}
    #             )
    #
    #     if not response.ok:
    #         raise RuntimeError("Connection interrupted.")
    #
    #     buffs = response.json()["auras"]
    #     count = 0
    #     for buff in buffs:
    #         if any(key == buff["name"] for
    #                     key in ["Potion of Prolonged Power",
    #                        "Potion of the Old War",
    #                        "Potion of Deadly Grace"]):
    #             count += buff["totalUses"]
    #     return count

    def ppLst(self, lst):
        if len(lst) == 0:
            return "(no one)"
        nLst = []
        for p in lst:
            nLst.append(self.pDict[p])
        nLst.sort()
        if len(nLst) == 1:
            return nLst[0]
        if len(nLst) == 2:
            return nLst[0] + " and " + nLst[1]
        s = ""
        i = 0
        for i in range(len(nLst) - 1):
            s += (nLst[i] + ", ")
        s += ("and " + nLst[-1])
        return s

    def raidMembers(self):
        pIDLst = []
        for pID in self.pDict:
            pIDLst.append(pID)
        return ("People who attended that night were " +
            self.ppLst(pIDLst))

    def potionCounts(self, pull = -1):
        oldwar = 188028
        prolonged = 229206
        deadlygrace = 188027

        dLst = self.dpsLst(pull)

        pCounts = {}
        for d in dLst:
            pCounts[d] = 0

        r1 = session.get(url + "report/tables/buffs/" + self.nightID,
                params = {"start": self.start(pull),
                          "end": self.end(pull), "abilityid": oldwar}
                )

        r2 = session.get(url + "report/tables/buffs/" + self.nightID,
                params = {"start": self.start(pull),
                          "end": self.end(pull), "abilityid": prolonged}
                )

        r3 = session.get(url + "report/tables/buffs/" + self.nightID,
                params = {"start": self.start(pull),
                          "end": self.end(pull), "abilityid": deadlygrace}
                )

        if not (r1.ok and r2.ok and r3.ok):
            raise RuntimeError("Connection interrupted.")

        for use in r1.json()["auras"]:
            if use["id"] in pCounts:
                pCounts[use["id"]] += use["totalUses"]

        for use in r2.json()["auras"]:
            if use["id"] in pCounts:
                pCounts[use["id"]] += use["totalUses"]

        for use in r3.json()["auras"]:
            if use["id"] in pCounts:
                pCounts[use["id"]] += use["totalUses"]
        return pCounts

    def ppPCounts(self, pCounts):
        rLst = []
        for pID in pCounts:
            rLst.append(self.pDict[pID] + " used " + '%d' % pCounts[pID] + ".")
        return rLst

    def potionCountOutput(self, pull = -1):
        return self.ppPCounts(self.potionCounts(pull))

    def noPotionList(self, pull = -1):
        if pull >= len(self.pulls):
            return "We didn't do that many pulls."
        pCounts = self.potionCounts(pull)
        lst = []
        for pID in pCounts:
            if pCounts[pID] == 0:
                lst.append(pID)
        if len(lst) == 0:
            return "Everyone used at least one potion."
        return ("People who didn't pot up were " +
                    self.ppLst(lst) + ".")

    def avg(self, pCounts):
        if len(pCounts) == 0:
            return 0
        pSum = 0
        for pID in pCounts:
            pSum += pCounts[pID]
        return pSum / len(pCounts)

    def pullPotionAnalysis(self, pull = -1):
        pCounts = self.potionCounts(pull)
        average = self.avg(pCounts)
        ans = {"average" : average}
        for pID in pCounts:
            if average - pCounts[pID] > 0.5:
                ans[self.pDict[pID]] = pCounts[pID]
        return ans

    async def nightPotionAnalysis(self):
        pcs = []
        avgs = []
        pPulls = {}

        numPulls = len(self.pulls)
        for i in range(numPulls):
            pc = self.potionCounts(i)
            pcs.append(pc)
            avgs.append(self.avg(pc))
            for key in pc:
                if key not in pPulls:
                    pPulls[key] = [i]
                else:
                    pPulls[key].append(i)

        lacks = {}

        ag = 0
        for p in pPulls:
            sp = 0
            sg = 0
            for pull in pPulls[p]:
                sp += pcs[pull][p]
                sg += avgs[pull]
            ap = sp / len(pPulls[p])
            ag = sg / len(pPulls[p])
            if ap - ag < -0.3:
                lacks[p] = (ap, ag)

        if len(lacks) == 0:
            return ["Nobody was falling behind in potion usage. "
                    "Our dps were using " + "%.2f" % ag
                    + " potions on average per pull."]

        lackLst = []
        for pID in lacks:
            lackLst.append((self.pDict[pID], lacks[pID], len(pPulls[pID])))
        lackLst.sort()

        ans = []
        for item in lackLst:
            ans.append(item[0] + " used " + '%.2f' % item[1][0] +
                       " potions per pull (out of " + "%d" % item[2] +
                       " pulls), while the group used " + '%.2f' % item[1][1]
                       + " on those fights.")
        return ans
