import discord
import asyncio
import random
import requests
import Log
import FaithData
from enum import Enum
from datetime import datetime

class Faith(discord.Client):

    class convoStatus(Enum):
        NO_CONVO = 0
        ACTIVE = 1
        PASSIVE = 2
        SPEC_FOR_STATS = 3
        SPEC_FOR_NECK = 4
        SUGGESTING = 5
        LOGS_ANALYSIS = 6

    conversations = {}

    async def disengageCheck(self, message):

        if (message.content.lower().startswith('ty')
            and len(message.content) <= 6):
            return True

        if ('thank' in message.content.lower()
                and len(message.content) < 15):
            return True

        if ("nothing" in message.content.lower()
                and len(message.content) < 10):
            return True

        if ((message.content.lower().startswith("no")
                or message.content.lower().startswith("nah"))
                and len(message.content) < 5):
            return True

        keywords = ["that's all", "that's it", "dismissed",
                    "that is all", "that is it",
                    "that's about it", "that's about all",
                    "that is about it", "that is about all",
                    "that should be all", "that should be it",
                    "never mind", "nevermind", "nvm",
                    "shut up", "go away",
                    "didn't ask", "did not ask",
                    "t asking you",
                    "t talking to you"]

        return any(key in message.content.lower()
                        for key in keywords)

    async def disengage(self, message):

        neutralKeys = ["that's all", "that's it", "dismissed",
                       "that is all", "that is it",
                       "never mind", "nevermind", "nvm",
                       "that's about it", "that's about all",
                       "that is about it", "that is about all",
                       "that should be all", "that should be it",]

        if ('thank' in message.content.lower()
                or message.content.lower().startswith('ty')):
            await self.send_message(message.channel, "No problem!")
        elif ('never mind' in message.content.lower()
                or 'nothing' in message.content.lower()):
            await self.send_message(message.channel, "Ok.")
        elif any(key in message.content.lower()
               for key in neutralKeys):
            await self.send_message(message.channel,
                                random.choice(
                                    ["I hope that was helpful.",
                                     "Hope that helped!"]))
        else: #negative dismissals
            await self.send_message(message.channel,
                                random.choice(["I'm sorry!",
                                "Sorry! I'll be quiet."]))

        self.conversations[message.author] = Faith.convoStatus.NO_CONVO
        return

    async def bankCheck(self, message):
        return 'bank' in message.content.lower()

    async def bank(self, message):
        await self.send_message(message.channel,
                                FaithData.bankInfo)
        self.conversations[message.author] = \
            Faith.convoStatus.PASSIVE
        return

    async def greetCheck(self, message):
        nongreets = ['thank', 'help', 'have']

        if any(n in message.content.lower() for n in nongreets):
            return False
        elif ('Faith' in message.content
                and len(message.content) <= 15):
            return True
        else:
            greetings = ['hey', 'morning',
                         'afternoon', 'evening',
                         "what's up"]
            for g in greetings:
                cutoff = 16 if 'Faith' in message.content else 10
                if (g in message.content.lower()
                        and len(message.content) - len(g) <= cutoff):
                    return True
            return False

    async def greet(self, message):
        name = self.address(message.author)
        query = random.choice(
                    ["",
                     " How can I help you today?",
                     " How may I be of service?"])

        hour = datetime.now().hour
        goodhour = ("Good morning, " if hour >= 3 and hour < 12 else
                    "Good afternoon, " if hour >= 12 and hour < 18 else
                    "Good evening, ")

        await self.send_message(message.channel,
                    random.choice(["Hi, " + name + "!" + query,
                                   "Hello, " + name + "!" + query,
                                   goodhour + name + "!" + query,
                                   "Greetings, " + name + "." + query])
                                )
        self.conversations[message.author] = Faith.convoStatus.ACTIVE
        return

    async def introduceCheck(self, message):
        return (("introduc" in message.content.lower()
                    and "you" in message.content)
                or ("who are you" in message.content.lower()))

    async def introduce(self, message):
        await self.send_message(message.channel,
                                random.choice(FaithData.intros))
        self.conversations[message.author] = Faith.convoStatus.PASSIVE
        return

    async def tourCheck(self, message):
        keywords = ["tour", "show me around", "show us around"]
        return any(key in message.content.lower() for key in keywords)

    async def tour(self, message):
        await self.send_message(message.channel, FaithData.tourScript)
        self.conversations[message.author] = Faith.convoStatus.PASSIVE
        return

    async def creatorCheck(self, message):
        return ("who created you" in message.content.lower()
                    or "your creator" in message.content.lower()
                    or "who made you" in message.content.lower())

    async def creator(self, message):
        await self.send_message(message.channel,
                ("Ethan taught me some basics to start with. "
                    "From now on, my creator will be all of you though!"))
        self.conversations[message.author] = \
                        Faith.convoStatus.PASSIVE

    async def capabilityCheck(self, message):
        keywords = ["what can you do",
                    "what else can you do",
                    "!help", "what are you able to do",
                    "what else are you able to do"]
        return any(key in message.content.lower()
                    for key in keywords)

    async def capability(self, message):
        await self.send_message(message.channel,
                FaithData.capabilities)
        self.conversations[message.author] = \
                        Faith.convoStatus.PASSIVE

    async def defCheck(self, message):
        keywords = ["tell me about", "bit about",
                    "little about",
                    "what's", "what is",
                    "tell us about"]
        return any(key in message.content.lower()
                        for key in keywords)

    async def define(self, message):
        mStr = message.content.lower()
        if 'facebook' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Facebook'])
        elif 'bank' in mStr:
            await self.bank(message)
        elif 'guild' in mStr or 'unified' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Unified'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'hanabi' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Hanabi'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'ethan' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Ethan'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'stus' in mStr or 'about doin' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Doin'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'danny' in mStr or 'yaois' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Danny'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'my rank' in mStr:
            await self.send_message(message.channel,
                            ("Our raiders' ranks are "
                             "posted on our Facebook group."))
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'rank' in mStr or 'loot' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Rank'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'raid' in mStr:
            await self.send_message(message.channel,
                              FaithData.definitions['Raid'])
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        elif 'about you' in mStr:
            await self.introduce(message)
        elif (('stat' in mStr and 'pri' in mStr) or 'stats' in mStr):
            await self.statPrio(message)
        elif 'neck ench' in mStr:
            await self.neck(message)
        else:
            await self.send_message(message.channel,
                     "Sorry, I don't have any information on that.")
            self.conversations[message.author] = \
                Faith.convoStatus.PASSIVE
        return

    def address(self, user):
        specialAddr = {'219510574256488449': "Lord Yaois",
                     '229766287377563659': "Lady Yvis",
                     '227246864766992386': "Lady Rosaree",
                     '236966898468651009': "Lady Fozzyrock",
                     '190494385035673611': "Master Ethan",
                     '227208882240356354': "Sire Stus",
                     '228659623421411329': "Shadowblade Falivash"}

        officers = ['229783128481333249', #Chirco
                         '232324612040556544', #Drakania
                         '234124188510715905', #Juvelices
                         '234445330844876801'] #Sawbones

        if user.id in specialAddr:
            return specialAddr[user.id]
        if user.id in officers:
            return "Officer " + user.display_name
        return user.display_name

    async def suggestionCheck(self, message):
        if (message.author in self.conversations
                and (self.conversations[message.author]
                    == Faith.convoStatus.ACTIVE
                    or self.conversations[message.author]
                    == Faith.convoStatus.PASSIVE)):
            return 'suggest' in message.content.lower()
        return False

    async def suggestion(self, message):
        await self.send_message(message.channel,
                "Sorry, I can't handle suggestions yet.")
        self.conversations[message.author] = \
                        Faith.convoStatus.PASSIVE
        return

    def getSpec(self, message):
        for spec in FaithData.specAbbrevs:
            if any(abbr in message for abbr in FaithData.specAbbrevs[spec]):
                return spec
        return "unknown"

    async def statPrioCheck(self, message):
        return (message.author in self.conversations
                    and (self.conversations[message.author]
                        == Faith.convoStatus.ACTIVE
                        or self.conversations[message.author]
                        == Faith.convoStatus.PASSIVE)
                    and (('stat' in message.content.lower()
                          and 'pri' in message.content.lower())
                         or 'stats' in message.content.lower()))

    async def statPrio(self, message):
        spec = self.getSpec(message.content.lower())
        if spec == "unknown":
            await self.send_message(message.channel,
                    "What class/spec are you playing?")
            self.conversations[message.author] = \
                    Faith.convoStatus.SPEC_FOR_STATS
        else:
            iv = random.choice(["Icy Veins said \"",
                            "According to Icy Veins, \""])
            await self.send_message(message.channel,
                    iv + FaithData.statprios[spec] + "\".")
            self.conversations[message.author] = Faith.convoStatus.PASSIVE
        return

    async def replyStatsCheck(self, message):
        return (message.author in self.conversations
            and self.conversations[message.author]
            == Faith.convoStatus.SPEC_FOR_STATS)

    async def replyStats(self, message):
        spec = self.getSpec(message.content.lower())
        if spec == "unknown":
            await self.send_message(message.channel,
                    "I can't recognize that spec.")
        else:
            iv = random.choice(["Icy Veins said \"", "According to Icy Veins, \""])
            await self.send_message(message.channel,
                    iv + FaithData.statprios[spec] + "\".")
        self.conversations[message.author] = Faith.convoStatus.PASSIVE
        return

    async def neckCheck(self, message):
        return ('ench' in message.content.lower()
                    or 'neck' in message.content.lower())

    async def neck(self, message):
        spec = self.getSpec(message.content.lower())
        if spec == "unknown":
            await self.send_message(message.channel,
                    "What class/spec are you playing?")
            self.conversations[message.author] = Faith.convoStatus.SPEC_FOR_NECK
        else:
            iv = random.choice(["Icy Veins said \"", "According to Icy Veins, \""])
            await self.send_message(message.channel,
                    iv + FaithData.neckenchs[spec] + "\".")
            self.conversations[message.author] = Faith.convoStatus.PASSIVE
        return

    async def replyNeckCheck(self, message):
        return (message.author in self.conversations
            and self.conversations[message.author]
            == Faith.convoStatus.SPEC_FOR_NECK)

    async def replyNeck(self, message):
        spec = self.getSpec(message.content.lower())
        if spec == "unknown":
            await self.send_message(message.channel,
                    "I can't recognize that spec.")
        else:
            iv = random.choice(["Icy Veins said \"", "According to Icy Veins, \""])
            await self.send_message(message.channel,
                    iv + FaithData.neckenchs[spec] + "\".")
        self.conversations[message.author] = Faith.convoStatus.PASSIVE
        return

    async def mentionCheck(self, message):
        return (('faith' in message.content.lower() and
                    len(message.content) < 15)
                or ('Faith' in message.content))

    async def mention(self, message):
        await self.send_message(message.channel,
                    self.address(message.author) +
                    ", did you call me?")
        self.conversations[message.author] = \
            Faith.convoStatus.PASSIVE

    async def wclogCheck(self, message):
        return ('warcraftlogs.com' in message.content.lower()
            or all(key in message.content.lower()
                for key in ['analy', 'log']))

    async def analysisCheck(self, message):
        return (message.author in self.conversations and
                self.conversations[message.author] == \
            Faith.convoStatus.LOGS_ANALYSIS)

    currentLog = {}

    async def wclog(self, message):

        try:
            try:
                loc = (message.content.lower()
                       .index('warcraftlogs.com/reports/')
                       + len('warcraftlogs.com/reports/'))
                self.currentLog[message.author] = \
                        Log.Night(message.content[loc:loc+16])
            except ValueError:
                self.currentLog[message.author] = \
                        Log.Night(Log.lastRaid())
        except RuntimeError:
            await self.send_message(message.channel,
                            "I can't access that log. "
                            "Please double check the link.")
            return
        await self.send_message(message.channel,
                           self.currentLog[message.author])
        self.conversations[message.author] = \
                Faith.convoStatus.LOGS_ANALYSIS
        return

    def getNum(self, s):
        seen = False
        val = 0
        for c in s:
            if not c.isdigit() and seen:
                return val - 1
            if c.isdigit():
                val *= 10
                val += int(c)
                seen = True
        return val - 1

    async def refreshCheck(self, message):
        return 'refresh' in message.content.lower()

    async def refreshLog(self, message):
        self.currentLog[message.author] = \
            Log.Night(self.currentLog[message.author].nightID)
        await self.send_message(message.channel, "Done refreshing.")
        self.conversations[message.author] = \
            Faith.convoStatus.LOGS_ANALYSIS
        return

    async def logCheck(self, message):
        return 'warcraftlogs.com/reports/' in message.content.lower()

    async def newLog(self, message):
        loc = (message.content.lower()
               .index('warcraftlogs.com/reports/')
               + len('warcraftlogs.com/reports/'))
        self.currentLog[message.author] = \
                Log.Night(message.content[loc:loc+16])
        await self.send_message(message.channel, "Switched over to " +
                            str(self.currentLog[message.author]))
        self.conversations[message.author] = \
            Faith.convoStatus.LOGS_ANALYSIS
        return

    async def pullDescCheck(self, message):
        keywords = ["tell me about", "bit about",
                    "little about",
                    "what's", "what is", "what was",
                    "tell us about"]
        return any(key in message.content.lower()
                            for key in keywords)

    async def pullDesc(self, message):
        pull = self.getNum(message.content)
        await self.send_message(message.channel,
            self.currentLog[message.author].pullDesc(pull))
        return

    async def noPotsCheck(self, message):
        return any(key in message.content.lower()
                    for key in ["t pot", "t use pot",
                                "no pot", "t use any pot"])

    async def noPots(self, message):
        pull = self.getNum(message.content)
        await self.send_message(message.channel,
            self.currentLog[message.author].noPotionList(pull))
        return

    async def potCountsCheck(self, message):
        return any(key in message.content.lower()
                    for key in ["pot count", "potion count",
                        "potions count", "potions usage",
                        "pot usage", "potion usage"])

    async def potCounts(self, message):
        pull = self.getNum(message.content)
        displayedPull = (pull + 1 if pull >= 0 else
                pull + 1 + len(self.currentLog[message.author].pulls))
        await self.send_message(message.channel,
            "Counting the potion usage for pull #" + str(displayedPull) + "...")
        for s in self.currentLog[message.author].potionCountOutput(pull):
            await self.send_message(message.channel, s)
        await self.send_message(message.channel, "That's all.")
        return

    async def nightPotCheck(self, message):
        return ('pot' in message.content.lower() and
                any(key in message.content.lower()
                    for key in ["night", "raid"]))

    async def nightPot(self, message):
        reply = random.choice(["Please give me a second.",
                               "Working on it!"])
        await self.send_message(message.channel,
                reply)
        analysis = await self.currentLog[
                        message.author].nightPotionAnalysis()
        for s in analysis:
            await self.send_message(message.channel, s)
        await self.send_message(message.channel, "That's all.")
        return

    async def analysisGreet(self, message):
        await self.send_message(message.channel,
                    "Yes, " + self.address(message.author)
                    + "? Sorry, I was looking at the logs.")
        self.conversations[message.author] = \
                Faith.convoStatus.ACTIVE

    async def memberCheck(self, message):
        return any(key in message.content.lower()
                for key in ["who were there", "member",
                        "participat"])

    async def raidMembers(self, message):
        await self.send_message(message.channel,
            self.currentLog[message.author].raidMembers())
        return

    async def numPullsCheck(self, message):
        return any(key in message.content.lower()
            for key in ["how many pulls", "how many fights"])

    async def numPulls(self, message):
        await self.send_message(message.channel,
            "We did "
            + str(len(self.currentLog[message.author].pulls))
            + " pulls.")

    analysisChecks = [disengageCheck, refreshCheck,
                logCheck, pullDescCheck, noPotsCheck,
                potCountsCheck, nightPotCheck, memberCheck,
                numPullsCheck, greetCheck]

    analysisReacts = {disengageCheck: disengage,
                        refreshCheck: refreshLog,
                        logCheck: newLog,
                        pullDescCheck: pullDesc,
                        noPotsCheck: noPots,
                        potCountsCheck: potCounts,
                        nightPotCheck: nightPot,
                        numPullsCheck: numPulls,
                        memberCheck: raidMembers,
                        greetCheck: analysisGreet}

    async def analyze(self, message):
        try:
            for check in self.analysisChecks:
                if await check(self, message):
                    await (self.analysisReacts[check])(self, message)
                    self.attention[message.author] = 3
                    return
        except RuntimeError:
            await self.send_message(message.channel,
                    "Sorry, we've overloaded our connection "
                    "with Warcraft Logs or typed in a wrong link. "
                    "Please try again later.")
            return

        #Does NOT understand
        await self.send_message(message.channel,
                    "I'm sorry, we don't have that "
                    "log analysis function yet.")
        return

    stateChecks = [suggestionCheck, replyStatsCheck,
                   replyNeckCheck, analysisCheck]

    checks = [greetCheck, introduceCheck,
              creatorCheck, capabilityCheck,
              defCheck, wclogCheck,
              statPrioCheck, neckCheck,
              tourCheck, bankCheck,
              disengageCheck,
              mentionCheck]

    react = {disengageCheck: disengage,
                replyStatsCheck: replyStats,
                replyNeckCheck: replyNeck,
                greetCheck: greet,
                introduceCheck: introduce,
                defCheck: define,
                capabilityCheck: capability,
                creatorCheck: creator,
                suggestionCheck: suggestion,
                statPrioCheck: statPrio,
                neckCheck: neck,
                tourCheck: tour,
                bankCheck: bank,
                mentionCheck: mention,
                wclogCheck: wclog,
                analysisCheck: analyze}

    async def on_ready(self):
        print("Yawn... Good morning?\n")
        return

    attention = {}

    def attentionCheck(self, message):
        if ('Faith' in message.content
            and (message.author not in self.conversations or
             self.conversations[message.author]
                == Faith.convoStatus.NO_CONVO)): #if mentioned
            self.conversations[message.author] = \
                Faith.convoStatus.ACTIVE

        if ('faith' in message.content
            and (message.author not in self.conversations or
             self.conversations[message.author]
                == Faith.convoStatus.NO_CONVO)):
            self.conversations[message.author] = \
                        Faith.convoStatus.PASSIVE
            if(message.author not in self.attention
                        or self.attention[message.author] < 1):
                self.attention[message.author] = 1

    async def on_message(self, message):

        if message.author == self.user:
            return

        #attention math
        self.attentionCheck(message)

        #special listen states
        for check in self.stateChecks:
            if await check(self, message):
                await (self.react[check])(self, message)
                self.attention[message.author] = 4
                return

        #regular conversations
        if (message.author in self.conversations
                and (self.conversations[message.author]
                    == Faith.convoStatus.ACTIVE
                    or self.conversations[message.author]
                    == Faith.convoStatus.PASSIVE)):
            for check in self.checks:
                if await check(self, message):
                    await (self.react[check])(self, message)
                    self.attention[message.author] = 4
                    return

        #does NOT understand
        if message.author in self.conversations:
            if (self.conversations[message.author]
                    == Faith.convoStatus.ACTIVE):
                await self.send_message(message.channel,
                      ("I'm sorry, "
                       "I can't understand that."))
                self.conversations[message.author] = \
                    Faith.convoStatus.PASSIVE

        #more attention math
        for a in self.attention:
            self.attention[a] = self.attention[a] - 1
            if self.attention[a] <= 0:
                self.conversations[a] = \
                        Faith.convoStatus.NO_CONVO

lifeform = Faith()
lifeform.run("MzI4MjE0NTc0MzQxOTQ3NDEy.DDCj0Q.FEJ9ETa9ct-J0wSn4JePAjWOqww")
